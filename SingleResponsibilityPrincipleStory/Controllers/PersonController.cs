﻿using Microsoft.AspNetCore.Mvc;
using SingleResponsibilityPrincipleStory.Models;
using SingleResponsibilityPrincipleStory.Services;

namespace SingleResponsibilityPrincipleStory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {  
        private readonly PersonService personService;

        public PersonController(
            PersonService personService)
        {            
            this.personService = personService;
        }

        [HttpGet("contact")]
        public ActionResult<Person> FindByEmail(string email)
        {
            var person = personService.FindPersonByEmail(email);
            if (person != null)
                return Ok(person);
            return NotFound("Contact not found");
        }

        [HttpPost("contact")]
        public ActionResult<Person> StorePerson(Person person)
        {
            return Ok(personService.StorePerson(person));
        }
    }
}
