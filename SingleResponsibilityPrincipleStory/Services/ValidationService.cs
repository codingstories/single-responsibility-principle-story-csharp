﻿using System.Linq;
using System.Text.RegularExpressions;
using SingleResponsibilityPrincipleStory.Models;

namespace SingleResponsibilityPrincipleStory.Services
{
    public class ValidationService
    {
        private const string EmailValidationPattern =
            @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

        public bool ValidatePerson(Person person)
        {
            if ((person?.Contacts?.Any(c => c.Type == Contact.ContactType.EMAIL) ?? false)
                && (!person.Contacts?.Any(c => c.Type == Contact.ContactType.EMAIL && !IsValidEmail(c.ContactData)) ??
                    false))
                return true;

            throw new ValidationException("Contact has no any valid emails");
        }

        public bool ValidateEmail(string email)
        {
            if (IsValidEmail(email))
                return true;

            throw new ValidationException("Email field is empty or invalid");
        }

        private bool IsValidEmail(string email)
        {
            return email != null && Regex.Match(email, EmailValidationPattern).Success;
        }
    }
}
